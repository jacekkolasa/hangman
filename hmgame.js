var io; 
var	gameSocket;
var firebaseRef;
var	words = new Array('abate','aberrant','abscond','accolade','cetic','assuage','astringent');
var word;
//firstGame - if true then if the game starts the list of players and names has to be made
var firstGame = true;

//list of players containing their socket-Id's [x][0], and their score [x][1] 
var listOfPlayers = []; //needs to be an object when multiple rooms are allowed
var currentPlayer = 0; //needs to be an object when multiple rooms are allowed


exports.init = function(sio, socket, myRootRef){
	io = sio;
    gameSocket = socket;
    firebaseRef = myRootRef;

    gameSocket.on('updateListOfPlayers', updateListOfPlayers);    
    gameSocket.on('prepareGame', prepareGame);
    gameSocket.on('playerJoinGame', playerJoinGame);
    gameSocket.on('letterChosen', letterChosen);
    gameSocket.on('gameOver', gameOver);
};



/**
 *  Player Events functions
 */

function playerJoinGame(data) {
	this.join(data.gameId);
	this.playerName = data.playerName;
	updateListOfPlayers(data.gameId);
	//emit to joined player to display start button, also emit player's ID and nickname
	this.emit('displayStartButton', {playerId : this.id, playerName : this.playerName});
}

//this function updates the list of players in waiting room
function updateListOfPlayers(gameId) {
	var clients = io.sockets.clients(gameId);
	var list = '';
	clients.forEach(function(client) {
		list = list + '<li>' + client.playerName + '</li>';
	});
	//emiting to all sockets (even outside the room) the list of logged players
	io.sockets.emit('updateListOfPlayers', list);
}


//function choose a word and send names and ids to all players in the room
function prepareGame(gameId) {
	//take a word from the list
	word = words[parseInt(Math.random()* words.length)];
	io.sockets.in(gameId).emit('wordReady', word);
	//restart the used letters string
	lettersUsed = '';
	if (firstGame) {
		var clients = io.sockets.clients(gameId);
		var i=0;
		var players = [];
		clients.forEach(function(client) {
			players[i] = new Array(2);
			players[i][0] = client.playerName;
			players[i][1] = client.id;
			listOfPlayers[i] = new Array(2);
			listOfPlayers[i][0] = players[i][1]; //a copy of Id for the server
			listOfPlayers[i][1] = 0; //scores of each individual player
			i++;
		});
		io.sockets.in(gameId).emit('firstGameMakeList', players);
		firstGame = false;
	}
}


// data = {letter, gameId}
function letterChosen(data) {
	var gameId = data.gameId;
	//check if letter was here before in this game
	if (lettersUsed.indexOf(data.letter) == -1) {
		lettersUsed += data.letter; //if this is a new letter
		letterRepeated = false;
	}
	else
		letterRepeated = true; //if letter has been here before
	
	//check if letter is good or wrong
	if (word.indexOf(data.letter) == -1) 
	//if the answer is wrong
	{
		//decrement the score by 5 points
		listOfPlayers[currentPlayer][1] -= 5;
		currentPlayer++;
		//the number of player cannot be the same as nr of players (first player is at [0])
		if (currentPlayer == listOfPlayers.length) currentPlayer = 0;
		data = {
				letterRepeated : letterRepeated,
				letter : data.letter,
				currentPlayer : listOfPlayers[currentPlayer][0],
				listOfPlayers : listOfPlayers
		};
		io.sockets.in(gameId).emit('badAnswer', data); //bad answer
	}
	
	
	//now, if the letter is good
	else 
	{
		//increment the score by 10 points
		listOfPlayers[currentPlayer][1] += 10;
		io.sockets.in(gameId).emit('goodAnswer', {letter : data.letter, listOfPlayers : listOfPlayers});		
	}	
}

function gameOver() {
	var userScoreRef = firebaseRef.child(this.playerName);
	var points = listOfPlayers[findPlayerById(this.id)][1];
	userScoreRef.setWithPriority({ name : this.playerName, points : points}, points);
}

//function returns the player number
function findPlayerById(playerId) {
	for (var i=0; i<listOfPlayers.length; i++) {
		if (listOfPlayers[i][0] == playerId) {
			return i;
		}
	}
}


