jQuery(function($){ //run the script when the document is ready
	var IO = {
			
			// Called when the page is displayed. 
			// Connects Socket.IO client to the Socket.IO server
			init: function() {
				IO.socket = io.connect();
				IO.bindEvents();
			},
			
			// Events connecting to the client
			bindEvents : function() {
				IO.socket.on('updateListOfPlayers', IO.updateListOfPlayers);
				IO.socket.on('displayStartButton', IO.displayStartButton);
				IO.socket.on('wordReady', IO.wordReady);
				IO.socket.on('firstGameMakeList', App.firstGameMakeList);
				IO.socket.on('badAnswer', App.badAnswer);
				IO.socket.on('goodAnswer', App.goodAnswer);	
			},
			
			
			//Updating the list of players. list variable is a string to be appended in <ul>
			updateListOfPlayers : function(list) {
				$('#listOfPlayers').html('List of players:'+list);
			},
			
			//data = {playerId, playerName}
			displayStartButton : function(data) {
				//place the Start button in place
				$('#startButtonTemplatePlace').html($('#startButtonTemplate').html());
				App.mySocketId = data.playerId;
				App.myName = data.playerName;
				console.log('Player ' + App.myName + ' looged in with ID: ' + App.mySocketId);
			},
			
			wordReady : function(word) {
				App.prepareGame(word);
			}
	};
	
	var App = {
			gameId : 1,//id of the game, so the server can support many games at the same time
			gameNr : 1,//counter for the current game(1 word = 1 game) 
			word : '',
			gallowsLvl : 0,
			mySocketId : 0,
			myName : '',
			underscores : '', //contains the word and/or underscores
			firstGame : true,
			//the number of best players displayed on leaderboard
			numOfLeaders : 3,
			//object with links to rows of the leader table 
			$leaderRows : {},
			firebaseSmall : {}, //this will be FB reference limited to {numOfLeaders} entries

			//number of players
			numPlayersInRoom : 0,
			noUsedLetters : true, //used at the beginning, if true, there is no coma before letter
			maxNumOfGames : 3,
			
			init: function() {
				App.cacheElements();
				App.showInitScreen();
				App.bindEvents();
	//			IO.socket.emit('updateListOfPlayers', App.gameId);
			},
			
			/**
			 *  Create references to on-screen elements used throughout the game
			 */
			
			cacheElements: function () {
				App.$doc = $(document);
				
				// Templates
				App.$mainBody = $('#mainBody');
				App.$loginTemplate = $('#loginTemplate').html();
				App.$mainGameTemplate = $('#mainGameTemplate').html();
			},
			
			bindEvents: function () {
				App.$doc.on('click', '#btnLogin', App.onJoinClick);
				App.$doc.on('click', '#startButton', App.onStartClick);
			},

			
			showInitScreen: function() {
				App.$mainBody.html(App.$loginTemplate);
			},
			
			showGameScreen: function() {
				App.$mainBody.html(App.$mainGameTemplate);
				App.wordField = document.getElementById('word');
				App.canvas = document.getElementById('game');
				//arrange the event when player press the enter key in input element
				$("#enterLetter").keypress(function(e) {
					if (e.which == 13) {
						App.letterChosen();
						//empty the input element, finding without jQuery is faster
						document.getElementById('enterLetter').value = '';
					}
				});
			},
			
			prepareGame: function(word) {
				//show the main game screen
				if (App.firstGame) {
					App.showGameScreen();
					App.firebaseInit(); //initialise the Firebase database
					//make a callback when player click restart button (next game)
					App.$doc.on('click', '#restartButton', App.onRestartClick);	
					}					
				App.word = word;
				//make an underscore field
				var wordLength = App.word.length;
				var underscores = '';
				$('#usedLetters').text('');
				for (var i=0; i<wordLength; i++)
					underscores += '_ ';
				App.underscores = underscores;
				App.wordField.innerHTML = underscores + ' hint: ' + word;//wywalic word stad pozniej	
				App.gallowsLvl = 0; //gallow at level 0
				App.canvas.width = App.canvas.width; //restart the canvas
				App.paintGallow(); //paint the canvas
				//used at the beginning, if true, there is no coma before letter			
				App.noUsedLetters = true;
				$('#game-counter').text(App.gameNr);
				App.firstGame = false;
				App.gameNr++;
				console.log("TERAZ GRA NR "+App.gameNr)
			},
			
			paintGallow: function() {
				var c = App.canvas.getContext('2d');
				// reset the canvas and set basic styles
				//App.canvas.width = App.canvas.width;
				c.lineWidth = 3;
				c.strokeStyle = 'black';
				//c.font = 'bold 24px Optimer, Arial, Helvetica, sans-serif';
				//c.fillStyle = 'red';
				switch(App.gallowsLvl)
				{
				case 0:
					// draw the ground
					App.drawLine(c, [1,190], [199,190]);
					// create the upright
					App.drawLine(c, [40,190], [40,9]);
					// create the arm of the gallows
					App.drawLine(c, [39,10], [146,10]);
					c.lineWidth = 3;
					// draw rope
					App.drawLine(c, [145,9], [145,30]);
					break;
				case 1:
					// draw head
					c.beginPath();
					c.moveTo(160, 45);
					c.arc(145, 45, 15, 0, (Math.PI/180)*360);
					c.stroke(); 
					break;
				case 2:
					// draw body
					App.drawLine(c, [145,60], [145,130]);
					break;
				case 3:
					// draw left arm
					App.drawLine(c, [145,80], [110,90]);
					break;
				case 4:
					// draw right arm
					App.drawLine(c, [145,80], [180,90]);
					break;
				case 5:
					// draw left leg
					App.drawLine(c, [145,130], [130,170]);
					break;
				case 6:
					// draw right leg and end game
					App.drawLine(c, [145,130], [160,170]);
					c.fillText('Game over', 45, 110);				
					break;
				}			
			},
			
			drawLine: function(context, from, to) {
				context.beginPath();
				context.moveTo(from[0], from[1]);
				context.lineTo(to[0], to[1]);
				context.stroke();
			},
			
			
			//when clicking the Choose button
			onJoinClick: function() {
				//$('#loginPlace').hide();
				var data = {
						gameId : App.gameId,
						playerName : $('#inputPlayerName').val(),
				};
				IO.socket.emit('playerJoinGame', data);
			},
			
			onStartClick: function() {
				
				IO.socket.emit('prepareGame', App.gameId);
			},
			
			//players[nrOfPlayer][0 - playerName, 1 - socketId]
			firstGameMakeList: function(players) {
				//make the list of online players - create <td> elements
				for (var i=0; i<players.length; i++) {
					$('<tr>')
					.appendTo('#online-board')
					.html('<td>' + players[i][0] + '</td> <td id="pointsOfPlayer_' + i + '">0</td>');
				}

				//now choose if this is first player
				if (App.mySocketId == players[0][1])
					App.youPlay();
				else
					App.youWait();
			},
			

			//if player press enter into the input field
			letterChosen: function() {
				var letter = document.getElementById('enterLetter').value;
				//trim spaces from the start and from the end
				letter = letter.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
				if (letter.length == 1) {
					//if there is only 1 symbol
					IO.socket.emit('letterChosen', {letter:letter, gameId:App.gameId});
				}
			},
			
			//data = {letterRepeated, letter, currentPlayer, listOfPlayers (in [x][1] there is score} 
			//this message goes also to waiting players
			badAnswer: function(data) {
				//if letter is '', then player guest wrongly the whole word
				if (data.letter != '') {
					if (App.noUsedLetters)
						$('#usedLetters').append(data.letter); //add this letter to used letters list
					else
						$('#usedLetters').append(', ' + data.letter); //add this letter to used letters list
				}
				App.noUsedLetters = false;
				App.gallowsLvl++;
				App.paintGallow();
				if (data.currentPlayer == App.mySocketId)
					App.youPlay();
				else
					App.youWait();
				App.updateScore(data.listOfPlayers);
				
			},
			
			//data = [letter, listOfPlayers]
			goodAnswer: function(data) {
				if (App.noUsedLetters)
					$('#usedLetters').append(data.letter); //add this letter to used letters list
				else
					$('#usedLetters').append(', ' + data.letter); //add this letter to used letters list
				App.updateUnderscores(data.letter);
				App.noUsedLetters = false;
				App.updateScore(data.listOfPlayers);
			},
			
			youWait: function() {
				document.getElementById('enterLetter').disabled = true;
			},
			
			youPlay: function() {
				document.getElementById('enterLetter').disabled = false;
			},
	
			updateUnderscores: function(letter) {
				//making the underscores and letters hint again
				for (var i=0; i<App.word.length; i++) {
					if (App.word[i] == letter) {
						App.underscores = App.setCharAt(App.underscores, i*2, App.word[i]);
						App.underscores = App.setCharAt(App.underscores, i*2+1, ' ');
					}
				}
				//underscore variable have white spaces between words so to eliminate them:
				var underscoresTemp = App.underscores.replace(/\s/g, "");
				App.wordField.innerHTML = underscoresTemp.replace(/_/g, "_ ") + ' HINT: ' + App.word;
			},
			
			setCharAt: function(str,index,chr) {
			    if(index > str.length-1) return str;
			    return str.substr(0,index) + chr + str.substr(index+1);
			},
			
			//update the score baised on listOfPlayers parameter
			// listOfPlayers[x][1] <=> score
			// also checks if word have been guessed
			updateScore: function(listOfPlayers) {
				for (var i=0; i<listOfPlayers.length; i++) {
					$('#pointsOfPlayer_'+i).text(listOfPlayers[i][1]);
				}
				//now checks if word have been guessed
				if (App.underscores.indexOf('_') == -1) {
					App.wordGuessed();
				}
			},
			
			onRestartClick: function() {
				IO.socket.emit('prepareGame', App.gameId);
			},

			//when the word has been guessed
			wordGuessed: function() {
				if (App.gameNr == App.maxNumOfGames) {
					//if this is a last game
					console.log("GAME OVER");
					IO.socket.emit('gameOver');

				}
				else {
					//if this is not a last game
					$('#restartButton').show();
				}

			},


			/****************************************
			****** HANDLE LEADBOARD SECTION *********
			****************************************/

			firebaseInit: function() {
				//reference for firebase that receive callbacks just for last {numOfLeaders} scores
				App.firebaseSmall = new Firebase('https://hangman.firebaseio.com//scores').limit(App.numOfLeaders);

				//if there is new leader, ie. there is new child:				
				App.firebaseSmall.on('child_added', function (childSnapshot, prevChildName) {
					App.handleLeaderAdded(childSnapshot, prevChildName);
				});

				// if the score changes or moves
				App.firebaseSmall.on('child_removed', function (childSnapshot) {
					App.handleLeaderRemoved(childSnapshot);
				});

				App.firebaseSmall.on('child_moved', App.changedCallback);
				App.firebaseSmall.on('child_changed', App.changedCallback);
			},


			handleLeaderAdded: function(childSnapshot, prevChildName) {
				console.log('funkcja');
				//prepare the row
				var newLeaderRow = $('<tr/>');
				newLeaderRow.append($('<td/>').append($('<em/>').text(childSnapshot.val().name)));
				newLeaderRow.append($('<td/>').text(childSnapshot.val().points));

				//store the link to the row in our object
				//snapshot.name() is the name of the snapshot (child)
				App.$leaderRows[childSnapshot.name()] = newLeaderRow;
				console.log(childSnapshot.name());
				//inserting the newLeaderRow in the document
				if (prevChildName === null) {
					console.log('previous byla null');
					//if there is empty place for this row
					$('#leader-board').append(newLeaderRow);
				}
				else {
					//moving the new row before the old one (they are sorted)
					var $lowerLeaderRow = App.$leaderRows[prevChildName];
					$lowerLeaderRow.before(newLeaderRow);
				}

			},

			//function for removing a row
			handleLeaderRemoved: function(childSnapshot) {
				var $removedLeaderRow = App.$leaderRows[childSnapshot.name()];
				$removedLeaderRow.remove();//deletes from table in document
				//deletes from object
				delete App.$leaderRows[childSnapshot.name()];
			},

			//add a callback to handle when a score changes or moves
			changedCallback: function(childSnapshot, prevChildName) {
				App.handleLeaderRemoved(childSnapshot);
				App.handleLeaderAdded(childSnapshot, prevChildName);
			}



			
	};
	
	IO.init();
	App.init();

}($));
