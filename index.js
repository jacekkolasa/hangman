// Import the Express module and create new instance of Express
var express = require('express');
var app = express();

// Import the 'path' module (packaged with Node.js)
var path = require('path');

// Import the server part of the game
var hmgame = require('./hmgame.js');

//Create a Node.js based http server on port 8080
var server = require('http').createServer(app);

//Create a Socket.IO server and attach it to the http server
var io = require('socket.io').listen(server);

var Firebase = require('firebase');
var myRootRef = new Firebase('https://hangman.firebaseio.com//scores');

server.listen(8080);

app.configure(function() {
    // Turn down the logging activity
    //app.use(express.logger('dev'));

    // Serve static html, js, css, and image files from the 'public' directory
    app.use(express.static(path.join(__dirname,'public')));
});

// Reduce the logging output of Socket.IO
io.set('log level', 2);

// Listen for Socket.IO Connections. Once connected, start the game logic.
io.sockets.on('connection', function (socket) {
    hmgame.init(io, socket, myRootRef);
});


